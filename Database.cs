﻿using AnalyticsDAO.DAO;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace AnalyticsDAO
{
    public abstract class Database
    {
        public abstract void InsertOne<T>(BaseDAO<T> dao, T record);
        public abstract void InsertMany<T>(BaseDAO<T> dao, List<T> records);

        public abstract List<T> ReadAll<T>(BaseDAO<T> dao);
        public abstract List<T> Read<T>(BaseDAO<T> dao, Expression<Func<T, bool>> filterExpression);
    }
}
