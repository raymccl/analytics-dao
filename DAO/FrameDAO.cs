﻿using AnalyticsDAO.Models;

namespace AnalyticsDAO.DAO
{
    public class FrameDAO : BaseDAO<Frame>
    {
        const string collectionName = "frame";

        public override string GetCollection()
        {
            return collectionName;
        }
    }
}
