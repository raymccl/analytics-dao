﻿using AnalyticsDAO.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace AnalyticsDAO.DAO
{
    /// <summary>
    /// The SessionDAO associated for accessing Session data.
    /// </summary>
    public class SessionDAO : BaseDAO<Session>
    {
        /// <summary>
        /// The name of the collection on the database.
        /// </summary>
        const string collectionName = "sessions";

        /// <summary>
        /// Returns the collection name associated with the DAO.
        /// </summary>
        /// <returns>The collection name.</returns>
        public override string GetCollection ()
        {
            return collectionName;
        }
    }
}
