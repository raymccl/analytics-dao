﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace AnalyticsDAO.DAO
{
    /// <summary>
    /// The BaseDAO model all DAO extend from.
    /// </summary>
    public abstract class BaseDAO<Model>
    {
        public abstract string GetCollection();

        /// <summary>
        /// Inserts the model into the database.
        /// </summary>
        /// <param name="db">The database the model should be inserted in.</param>
        /// <param name="model">The model to insert.</param>
        public virtual void InsertOne(Database db, Model model)
        {
            db.InsertOne(this, model);
        }

        /// <summary>
        /// Inserts the models into the database.
        /// </summary>
        /// <param name="db">The database the models should be inserted in.</param>
        /// <param name="models">A list of models to insert.</param>
        public virtual void InsertMany(Database db, List<Model> models)
        {
            db.InsertMany(this, models);
        }

        /// <summary>
        /// Reads all entries in the database of the given type and returns a list.
        /// </summary>
        /// <param name="db">The database to read entries from.</param>
        /// <returns>A list of all entries.</returns>
        public virtual List<Model> ReadAll(Database db)
        {
            return db.ReadAll(this);
        }

        /// <summary>
        /// Reads the database for all entries matching the filter expression.
        /// </summary>
        /// <param name="db">The database to read entries from.</param>
        /// <param name="filterExpression">An expression to filter down the entries for a given criteria.</param>
        /// <returns>A list of all entries matching the filterExpression.</returns>
        public virtual List<Model> Read(Database db, Expression<Func<Model, bool>> filterExpression)
        {
            return db.Read(this, filterExpression);
        }
    }
}
