﻿namespace AnalyticsDAO
{
    public abstract class DBClient
    {
        public abstract Database GetDatabase(string database);
    }

}
