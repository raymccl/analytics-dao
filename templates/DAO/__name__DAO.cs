using AnalyticsDAO.Models;

namespace AnalyticsDAO.DAO
{
    public class {{name}}DAO : BaseDAO<{{name}}>
    {
        const string collectionName = "{{name}}";

        public override string GetCollection()
        {
            return collectionName;
        }
    }
}
