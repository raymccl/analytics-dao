﻿using AnalyticsDAO.DAO;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace AnalyticsDAO
{
    class MongoDatabase : Database
    {
        IMongoDatabase mongoDb;
        public MongoDatabase (IMongoDatabase mongoDb)
        {
            this.mongoDb = mongoDb;
        }

        public override void InsertOne<T>(BaseDAO<T> dao, T record)
        {
            GetCollection(dao).InsertOne(record);
        }

        public override void InsertMany<T>(BaseDAO<T> dao, List<T> record)
        {
            GetCollection(dao).InsertMany(record);
        }

        public override List<T> ReadAll<T>(BaseDAO<T> dao)
        {
            return GetCollection(dao).Find(new BsonDocument()).ToList();
        }

        public override List<T> Read<T>(BaseDAO<T> dao, Expression<Func<T, bool>> filterExpression)
        {
            return GetCollection(dao).Find(filterExpression).ToList();
        }

        private IMongoCollection<T> GetCollection<T>(BaseDAO<T> dao)
        {
            return mongoDb.GetCollection<T>(dao.GetCollection());
        }
    }
}
