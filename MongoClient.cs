﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;

namespace AnalyticsDAO
{
    public class MongoClient : DBClient
    {
        MongoDB.Driver.MongoClient dbClient;
        public MongoClient(string ip, string port, string username, string secret)
        {
            dbClient = new MongoDB.Driver.MongoClient("mongodb://" + username + ":" + secret + "@" + ip + ":" + port);
        }
        
        public override Database GetDatabase (string database)
        {
            return new MongoDatabase(dbClient.GetDatabase(database));
        }
    }
}
