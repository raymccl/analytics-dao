﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace AnalyticsDAO.Models
{
    public class Session
    {
        [BsonId]
        public Guid Id { get; set; }

        public string Origin { get; set; }

        public long Seed { get; set; }

        public long StartTime { get; set; }

        public List<Frame> Frames { get; set; }
    }
}
